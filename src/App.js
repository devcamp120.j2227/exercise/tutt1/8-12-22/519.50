import "bootstrap/dist/css/bootstrap.min.css";
import { Container } from 'reactstrap';
import Form from './components/Form/Form';

function App() {
  return (
    <Container className="mt-5">
      <Form/>
    </Container>
  );
}

export default App;
