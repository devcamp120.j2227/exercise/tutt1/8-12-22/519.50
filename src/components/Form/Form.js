import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Input, Label } from "reactstrap";

const Form = () => {
    return (
        <>
            <div className="row text-center">
                <h2>Registration form</h2>
            </div>
            <div className="row mt-4">
                <div className="col-sm-6">
                    <div className="row">
                        <div className="col-sm-4"><Label>First Name <span className="text-danger">(*)</span></Label></div>
                        <div className="col-sm-8"><Input type="text"/></div>
                    </div>
                </div>
                <div className="col-sm-6">
                    <div className="row">
                        <div className="col-sm-4"><Label>Passport <span className="text-danger">(*)</span></Label></div>
                        <div className="col-sm-8"><Input type="text"/></div>
                    </div>
                </div>
            </div>
            <div className="row mt-4">
                <div className="col-sm-6">
                    <div className="row">
                        <div className="col-sm-4"><Label>Last Name <span className="text-danger">(*)</span></Label></div>
                        <div className="col-sm-8"><Input type="text"/></div>
                    </div>
                </div>
                <div className="col-sm-6">
                    <div className="row">
                        <div className="col-sm-4"><Label>Email</Label></div>
                        <div className="col-sm-8"><Input type="email"/></div>
                    </div>
                </div>
            </div>
            <div className="row mt-4">
                <div className="col-sm-6">
                    <div className="row">
                        <div className="col-sm-4"><Label>Birth Day <span className="text-danger">(*)</span></Label></div>
                        <div className="col-sm-8"><Input type="text"/></div>
                    </div>
                </div>
                <div className="col-sm-6">
                    <div className="row">
                        <div className="col-sm-4"><Label>Country <span className="text-danger">(*)</span></Label></div>
                        <div className="col-sm-8"><Input type="text"/></div>
                    </div>
                </div>
            </div>
            <div className="row mt-4">
                <div className="col-sm-6">
                    <div className="row">
                        <div className="col-sm-4"><Label>Gender <span className="text-danger">(*)</span></Label></div>
                        <div className="col-sm-8">
                            <div className="row text-center">
                                <div className="col-sm-6">
                                    <Input type="checkbox"></Input>{" "} Male
                                </div>
                                <div className="col-sm-6">
                                    <Input type="checkbox"></Input>{" "} Female
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-sm-6">
                    <div className="row">
                        <div className="col-sm-4"><Label>Region</Label></div>
                        <div className="col-sm-8"><Input type="text"/></div>
                    </div>
                </div>
            </div>
            <div className="row mt-4">
                <div className="col-sm-2">
                    <Label>Subject</Label>
                </div>
                <div className="col-sm-10">
                    <Input type="textarea"/>
                </div>
            </div>
            <div className="row mt-4">
                <div className="col-sm-6"></div>
                <div className="col-sm-6">
                    <div className="row">
                        <div className="col-sm-6"><Button color="success" className="w-100">Check Data</Button></div>
                        <div className="col-sm-6"><Button color="success" className="w-100">Send</Button></div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Form